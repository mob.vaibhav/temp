import os

PROJECT_DIR = os.path.abspath(os.path.dirname(__file__))
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(PROJECT_DIR, 'tmp/temp.sqlite'),
    }
}
INSTALLED_APPS = ('insurance_policy.apps.InsurancePolicyApp',)

SECRET_KEY = 'REPLACE_ME'
