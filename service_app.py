import os
from flask import Flask
from django.apps import apps
from flask_restful import Api

os.environ["DJANGO_SETTINGS_MODULE"] = "settings"
from django.conf import settings

apps.populate(settings.INSTALLED_APPS)
app = Flask(__name__)
app.config.from_pyfile("config.ini")

# Api's
api = Api(app)

from http_controllers.customer_policy import CustomerPolicySearch, CustomerPolicyEdit
from http_controllers.customer_policy_analytics import CustomerPolicyAnalytics

api.add_resource(CustomerPolicySearch, '/searchId/', '/searchId/<string:policy_id>')
api.add_resource(CustomerPolicyEdit, '/insertInsurerData/', '/insertInsurerData/<string:policy_id>')
api.add_resource(CustomerPolicyAnalytics, '/monthlyStatistics/', '/monthlyStatistics/<string:region>')

if __name__ == '__main__':
    app.run(port=5000)
