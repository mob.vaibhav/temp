from flask import request

from insurance_policy.dao.customer_policy import get_customer_policy
from insurance_policy.dao.policy import update_policy
from insurance_policy.views.customer_policy import multiple_view
from .base_resource import BaseResource
from django.utils.timezone import datetime
from re import sub


class CustomerPolicySearch(BaseResource):
    def get(self, policy_id=None):
        count, customer_policy_list = get_customer_policy(
            policy_id=policy_id,
        )
        results = multiple_view(customer_policy_list)
        return results


class CustomerPolicyEdit(BaseResource):

    def put(self, policy_id=None):
        request_data = request.get_json(force=True)
        insurer_data = request_data['insurerData']
        # not in current req.
        date_of_purchase = datetime.strptime(insurer_data['dateOfPurchase'], '%m/%d/%Y')

        fuel = insurer_data['fuel']
        policy_id = policy_id or insurer_data['policyId']
        vehicle_segment = insurer_data['vehicleSegment']
        premium = int(insurer_data['premium'].replace('$', ''))
        has_bodily_injury_liability = bool(int(insurer_data['bodilyInjuryLiability']))
        has_personal_injury_liability = bool(int(insurer_data['personalInjuryLiability']))
        has_property_damage_liability = bool(int(insurer_data['propertyDamageLiability']))
        has_collision = bool(int(insurer_data['collision']))
        has_comprehensive = bool(int(insurer_data['comprehensive']))

        update_policy(
            policy_id=policy_id,
            fuel=fuel,
            vehicle_segment=vehicle_segment,
            premium=premium,
            has_bodily_injury_liability=has_bodily_injury_liability,
            has_personal_injury_liability=has_personal_injury_liability,
            has_property_damage_liability=has_property_damage_liability,
            has_collision=has_collision,
            has_comprehensive=has_comprehensive,
        )

        # not in current req.
        customer_gender = insurer_data['customerGender']
        customer_id = insurer_data['customerId']
        customer_income_group = insurer_data['customerIncomeGroup']
        customer_region = insurer_data['customerRegion']
        is_married = bool(int(insurer_data['customerMaritalStatus']))

        return {"isUpdated": True}
