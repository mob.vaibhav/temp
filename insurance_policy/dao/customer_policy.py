from django.db.models import Count
from django.db.models.functions import TruncMonth

from insurance_policy.models import CustomerPolicy


def get_customer_policy(policy_id=None, limit=None, offset=0):
    filters = {}
    if policy_id:
        filters['policy_id'] = policy_id
    queryset = CustomerPolicy.objects.filter(
        **filters
    ).select_related(
        'policy', 'customer_history'
    )
    if limit:
        data, count = list(queryset[offset:limit + offset]), queryset.count()
    else:
        data = list(queryset)
        count = len(data)
    return count, data


def get_monthly_aggregated_list_for_region(region):
    aggregated_list = list(
        CustomerPolicy.objects.filter(
            customer_history__region=region.upper(),
        ).annotate(
            month=TruncMonth('date_of_purchase')
        ).values(
            # groupBy
            'month',
        ).annotate(
            count=Count('pk')
        ).values(
            'month', 'count'
        )
    )
    for x in aggregated_list:
        x['month'] = x['month'].month

    return aggregated_list