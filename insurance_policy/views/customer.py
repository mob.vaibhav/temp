from utils.case_conversion import bool_in_str


def customer_single_view(customer):
    return dict(
        customer_gender=customer.gender,
        customer_income_group=customer.income_group,
        customer_region=customer.region,
        customer_phone=customer.phone,
        customer_marital_status=bool_in_str(customer.is_married)
    )
