from django.db.models import IntegerField, CharField, BooleanField, ForeignKey, CASCADE
from .base import BaseModel


class AbstractCustomer(BaseModel):
    gender_choices = [
        ('M', 'MALE'),
        ('F', 'FEMALE'),
    ]
    gender = CharField(max_length=2, default=None, choices=gender_choices, null=True)
    is_married = BooleanField(null=True, default=None)
    region_choices = [
        ('E', 'EAST'),
        ('W', 'WEST'),
        ('N', 'NORTH'),
        ('S', 'SOUTH'),
    ]
    region = CharField(max_length=2, null=True, default=None, choices=region_choices)
    income_group = CharField(max_length=32, default=None, null=True)
    phone = CharField(max_length=13, default=None, null=True)

    class Meta:
        abstract = True


class Customer(AbstractCustomer):
    @property
    def latest_history_instance(self):
        return CustomerHistory.objects.filter(
            customer_id=self.pk
        ).latest(
            'created_at'
        )

    def save(self, *args, **kwargs):
        if self.region:
            self.region = self.region.upper()
        fields = self.fields_name_list
        history_required = False
        history_kwargs = {}
        for field in fields:
            if self.original_value.get(field) != getattr(self, field):
                history_required = True
                history_kwargs[field] = getattr(self, field)

        super(Customer, self).save(*args, **kwargs)
        if history_required:
            CustomerHistory.objects.create(customer_id=self.pk, **history_kwargs)


class CustomerHistory(AbstractCustomer):
    customer = ForeignKey(Customer, on_delete=CASCADE)
