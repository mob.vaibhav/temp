from insurance_policy.views.customer import customer_single_view
from insurance_policy.views.policy import single_policy_view


def single_view(customer_policy):
    policy = customer_policy.policy
    customer_history = customer_policy.customer_history
    view = dict(
        policy_id=customer_policy.policy_id,
        customer_id=customer_history.customer_id,
        date_of_purchase=customer_policy.date_of_purchase.strftime('%m/%d/%Y')
    )
    view.update(single_policy_view(policy))
    view.update(customer_single_view(customer_history))
    return view


def multiple_view(customer_policies):
    return [single_view(customer_policy) for customer_policy in customer_policies]
