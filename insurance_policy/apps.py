from django.apps import AppConfig


class InsurancePolicyApp(AppConfig):
    name = 'insurance_policy'

    def ready(self):
        # binding signals with django
        pass