import traceback
import decimal
from functools import wraps
from django import db
from flask import current_app
from flask_restful import Resource
from datetime import datetime
from uuid import UUID

from utils.case_conversion import convert_snake_to_camel


def cors(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            data, status, headers = func(*args, **kwargs)

            cors_allow_headers = ', '.join(current_app.config.get('CORS_ALLOW_HEADERS', ["*"]))
            cors_allow_origins = ', '.join(current_app.config.get('CORS_ALLOW_ORIGINS', ["*"]))
            cors_allow_methods = ', '.join(current_app.config.get('CORS_ALLOW_METHODS', ["*"]))

            headers.update(
                {
                    'Access-Control-Allow-Headers': cors_allow_headers,
                    'Access-Control-Allow-Origin': cors_allow_origins,
                    'Access-Control-Allow-Methods': cors_allow_methods,
                    'content-type': 'application/json'
                }
            )
            return (data, status, headers)
        except Exception as e:
            print(traceback.print_exc())
        finally:
            db.connections.close_all()

    return wrapper


def update_to_supportable_client_json(obj):
    if isinstance(obj, UUID):
        return str(obj)
    elif isinstance(obj, list):
        return [update_to_supportable_client_json(x) for x in obj]
    elif isinstance(obj, dict):
        return {convert_snake_to_camel(k): update_to_supportable_client_json(v) for k, v in obj.items()}
    elif isinstance(obj, datetime):
        return str(obj)
    elif isinstance(obj, decimal.Decimal):
        return float(obj)
    elif obj is None:
        return None
    elif any([isinstance(obj, float), isinstance(obj, int)]):
        return obj
    return str(obj)


def sanitize_response(response):
    data = None
    headers = {}
    if isinstance(response, tuple):
        (data, http_status, headers) = response
    else:
        data, http_status = response, 200
    data = update_to_supportable_client_json(data)
    return (data, http_status, headers)


def format_response(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        data, http_status, headers = sanitize_response(func(*args, **kwargs))
        return (data, http_status, headers)

    return wrapper


class BaseResource(Resource):
    method_decorators = [format_response, cors]

    def options(self, *args, **kwargs):
        return {'status': True}
