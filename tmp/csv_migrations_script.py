import csv
import os

os.environ["DJANGO_SETTINGS_MODULE"] = "settings"

import django

django.setup()
from insurance_policy.models import *
from django.utils.timezone import datetime

with open('./tmp/records.csv') as f:
    reader = csv.DictReader(f)
    records = list(reader)
    for r in records:
        r['dateOfPurchase'] = datetime.strptime(r['dateOfPurchase'], '%m/%d/%Y')

    records = sorted(records, key=lambda x: x['dateOfPurchase'])
    for index, record in enumerate(records):
        policyId = record['policyId']
        dateOfPurchase = record['dateOfPurchase']
        customerId = record['customerId']
        fuel = record['fuel']
        vehicleSegment = record['vehicleSegment']
        premium = record['premium']
        bodilyInjuryLiability = record['bodilyInjuryLiability']
        personalInjuryLiability = record['personalInjuryLiability']
        propertyDamageLiability = record['propertyDamageLiability']
        collision = record['collision']
        comprehensive = record['comprehensive']
        customerGender = record['customerGender']
        customerIncomeGroup = record['customerIncomeGroup']
        customerRegion = record['customerRegion']
        customerMaritalStatus = record['customerMaritalStatus']
        customer_dict = dict(
            gender=customerGender,
            region=customerRegion,
            phone=None,
            income_group=customerIncomeGroup,
            is_married=bool(int(customerMaritalStatus))
        )

        customer = Customer.objects.filter(pk=customerId).first()
        if not customer:
            customer = Customer.objects.create(pk=customerId)

        for k, v in customer_dict.items():
            setattr(customer, k, v)
        customer.save()

        policy_dict = dict(
            pk=policyId,
            premium=premium,
            vehicle_segment=premium,
            fuel=fuel,
            has_bodily_injury_liability=bool(int(bodilyInjuryLiability)),
            has_personal_injury_liability=bool(int(personalInjuryLiability)),
            has_property_damage_liability=bool(int(propertyDamageLiability)),
            has_collision=bool(int(collision)),
            has_comprehensive=bool(int(comprehensive)),
        )
        policy = Policy.objects.filter(**policy_dict).first()
        if not policy:
            policy = Policy.objects.create(**policy_dict)

        customer_policy_dict = dict(
            customer_history=customer.latest_history_instance,
            policy=policy,
            date_of_purchase=dateOfPurchase
        )
        customer_policy = CustomerPolicy.objects.filter(**customer_policy_dict).first()
        if not customer_policy:
            customer_policy = CustomerPolicy.objects.create(**customer_policy_dict)

        print(index, len(records))

print(CustomerPolicy.objects.count(), 'success')
