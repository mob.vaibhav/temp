from flask import request

from insurance_policy.dao.customer_policy import get_customer_policy, get_monthly_aggregated_list_for_region
from insurance_policy.views.customer_policy import multiple_view
from .base_resource import BaseResource


class CustomerPolicyAnalytics(BaseResource):
    def get(self, region):
        results = get_monthly_aggregated_list_for_region(region)
        return results
