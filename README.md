###   doc ref.
flask restful 
* https://flask-restful.readthedocs.io/en/latest/quickstart.html#a-minimal-api
* http_controllers/base_resource.py
  * resource extended with wrapper https://flask-restful.readthedocs.io/en/latest/extending.html#resource-method-decorators
### pre-scripts to run
* python manage.py migrate     
* python tmp/csv_migrations_script.py (if using new empty sqlite db)

##  APIS
1. http://127.0.0.1:8000/monthlyStatistics/North
2. http://localhost:8000/searchId/12346

3. Endpoint :- http://127.0.0.1:8000/insertInsurerData

    Request:-
    
    {
        "insurerData" : {
            "policyId" : 12345,
            "dateOfPurchase" : "12/02/2020",
            "customerId" : 400,
            "fuel" : "CNG",
            "vehicleSegment" : "A",
            "premium" : "$1000",
            "bodilyInjuryLiability" : "0",
            "personalInjuryLiability" : "1",
            "propertyDamageLiability" : "1",
            "collision" : "0",
            "comprehensive" : "0",
            "customerGender" : "Male",
            "customerIncomeGroup" : "0-$25K",
            "customerRegion" : "North",
            "customerMaritalStatus" : "0"
        }
    }
    
    Response :-
    {
        "isUpdated" : true
    }

##  server run
python service_app.py

python used version 3.9
