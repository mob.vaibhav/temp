from django.db import models
from django.utils.timezone import datetime


class BaseModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    def __init__(self, *args, **kwargs):
        super(BaseModel, self).__init__(*args, **kwargs)
        self.original_value = self.__dict__.copy()

    @property
    def fields_name_list(self):
        return [
            x.attname for x in self._meta.fields
            if not x.primary_key
        ]

    def save(self, *args, **kwargs):
        self.updated_at = datetime.now()
        super(BaseModel, self).save(*args, **kwargs)

    class Meta:
        abstract = True
