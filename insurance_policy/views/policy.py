from utils.case_conversion import bool_in_str


def single_policy_view(policy):
    return dict(
        fuel=policy.fuel,
        vehicle_segment=policy.vehicle_segment,
        premium='${}'.format(policy.premium) if policy.premium else '',
        bodily_injury_liability=bool_in_str(policy.has_bodily_injury_liability),
        personal_injury_liability=bool_in_str(policy.has_personal_injury_liability),
        property_damage_liability=bool_in_str(policy.has_property_damage_liability),
        collision=bool_in_str(policy.has_collision),
        comprehensive=bool_in_str(policy.has_comprehensive)
    )
