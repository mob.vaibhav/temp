from insurance_policy.models import Policy


def get_policy_by_id(policy_id):
    return Policy.objects.get(pk=policy_id)


def update_policy(policy_id, **update_kwargs):
    policy = get_policy_by_id(policy_id)
    update_dict = {}
    for k, v in update_kwargs.items():
        prev_value = getattr(policy, k, None)
        if prev_value != v:
            update_dict[k] = v
    if update_dict:
        for k, v in update_dict.items():
            setattr(policy, k, v)
        policy.save()

    return True
