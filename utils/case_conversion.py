def convert_snake_to_camel(key):
    splits = key.split('_')
    return '{}{}'.format(splits[0], ''.join([x.title() for x in splits[1:]]))


def bool_in_str(_bool):
    return str(int(bool(_bool)))