from django.db.models import IntegerField, CharField, BooleanField, ForeignKey, DateTimeField, SET_NULL
from .base import BaseModel


class Policy(BaseModel):
    premium = IntegerField()
    vehicle_segment = CharField(max_length=1, default=None, null=True)
    fuel = CharField(max_length=12, default='CNG')
    has_bodily_injury_liability = BooleanField(default=False)
    has_personal_injury_liability = BooleanField(default=False)
    has_property_damage_liability = BooleanField(default=False)
    has_collision = BooleanField(default=False)
    has_comprehensive = BooleanField(default=False)


class CustomerPolicy(BaseModel):
    customer_history = ForeignKey('CustomerHistory', on_delete=SET_NULL, null=True)
    policy = ForeignKey('Policy', on_delete=SET_NULL, null=True)
    date_of_purchase = DateTimeField()
